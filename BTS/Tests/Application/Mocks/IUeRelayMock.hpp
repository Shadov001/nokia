#pragma once

#include <gmock/gmock.h>
#include "UeRelay/IUeRelay.hpp"
#include "Printers/UeSlotPrint.hpp"

namespace bts
{

class IUeRelayMock : public IUeRelay
{
public:
    IUeRelayMock();
    ~IUeRelayMock() override;

    UePtr lastAddedUe;

    MOCK_METHOD1(addMock, UeSlot(UePtr&)); // workaround to problem with mocking function accepting value of unique_ptr<T> type...
    UeSlot add(UePtr uePtr) override
    {
        lastAddedUe = std::move(uePtr);
        return addMock(lastAddedUe);
    }

    MOCK_CONST_METHOD0(count, std::size_t());
    MOCK_CONST_METHOD0(countAttached, std::size_t());
    MOCK_CONST_METHOD0(countNotAttached, std::size_t());

    MOCK_METHOD1(visitAttachedUe, void(UeVisitor));
    MOCK_METHOD1(visitNotAttachedUe, void(UeVisitor));

    MOCK_METHOD2(sendMessage, bool(BinaryMessage message, PhoneNumber to));


};


}
