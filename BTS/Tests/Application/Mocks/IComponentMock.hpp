#pragma once

#include <gmock/gmock.h>
#include "IComponent.hpp"

namespace bts
{

class IComponentMock : public IComponent
{
public:
    IComponentMock();
    ~IComponentMock() override;

    MOCK_METHOD0(start, void());
    MOCK_METHOD0(stop, void());
};


}
