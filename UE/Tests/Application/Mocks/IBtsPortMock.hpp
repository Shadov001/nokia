#pragma once

#include <gmock/gmock.h>
#include "Ports/IBtsPort.hpp"

namespace ue
{

class IBtsEventsHandlerMock : public IBtsEventsHandler
{
public:
    IBtsEventsHandlerMock();
    ~IBtsEventsHandlerMock() override;

    MOCK_METHOD1(handleSib, void(common::BtsId));
    MOCK_METHOD0(handleAttachAccept, void());
    MOCK_METHOD0(handleAttachReject, void());
};

class IBtsPortMock : public IBtsPort
{
public:
    IBtsPortMock();
    ~IBtsPortMock() override;

    MOCK_METHOD1(sendAttachRequest, void(common::BtsId));
};

}
